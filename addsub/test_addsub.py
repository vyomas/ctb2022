# See LICENSE.vyoma for details

import os
import random
from pathlib import Path

import cocotb
from cocotb.clock import Clock
from cocotb.triggers import FallingEdge


@cocotb.test()
async def test_addsub(dut):
    """Test for addsub"""
    print('Hai')
    clock = Clock(dut.clk, 10, units="us")  # Create a 10us period clock on port clk
    cocotb.start_soon(clock.start())  # Start the clock

    await FallingEdge(dut.clk)  # Synchronize with the clock
    in1 = random.randint(0, 15)
    in2 = random.randint(0, 15)
    add_sub = random.randint(0, 1)
    if in1 < in2:
      add_sub = 0
    expected_out = 0
    if add_sub == 0:
        expected_out = in1 + in2
    else:
        expected_out = in1 - in2
    dut.data_in1.value = in1
    dut.data_in2.value = in2
    dut.add_sub.value = add_sub  # Assign the random value val to the input port d
    await FallingEdge(dut.clk)
    assert int(dut.data_out.value) == int(expected_out), f'Error: data_out={dut.data_out.value} != expected_out={expected_out}'
