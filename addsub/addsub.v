module addsub (
  input [3:0] data_in1,
  input [3:0] data_in2,
  input add_sub,            // 0: add, 1: subtract
  input clk,
  output reg [4:0] data_out);

  always @(posedge clk)
  begin
    if(add_sub)
      data_out <= data_in1 - data_in2;
    else
      data_out <= data_in1 + data_in2;
  end
endmodule : addsub